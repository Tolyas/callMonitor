package com.frumscepend.callmonitor.main.service

import android.content.Context
import android.content.Context.MODE_PRIVATE
import android.content.SharedPreferences
import com.frumscepend.callmonitor.api.ApiClient
import com.frumscepend.callmonitor.api.IncomigCallAlert
import com.frumscepend.callmonitor.api.RemoteApi
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

import java.util.Date

class CallReceiver : PhonecallReceiver() {
    private val apiClient = ApiClient.getClient().create(RemoteApi::class.java)
    private lateinit var sharedPref: SharedPreferences

    override fun onIncomingCallReceived(ctx: Context, number: String, start: Date) {
        sharedPref = ctx.getSharedPreferences("userdetails", MODE_PRIVATE)
        val incomigCallAlert = IncomigCallAlert().apply {
            key = sharedPref.getString("key", "")
            phoneNumber = number
        }
        apiClient.setIncomigCallAlert(incomigCallAlert).enqueue(object: Callback<Void> {
            override fun onFailure(call: Call<Void>, t: Throwable) {

            }

            override fun onResponse(call: Call<Void>, response: Response<Void>) {

            }

        })
    }

    override fun onIncomingCallAnswered(ctx: Context, number: String, start: Date) {
        //
    }

    override fun onIncomingCallEnded(ctx: Context, number: String, start: Date, end: Date) {
        //
    }

    override fun onOutgoingCallStarted(ctx: Context, number: String, start: Date) {
        //
    }

    override fun onOutgoingCallEnded(ctx: Context, number: String, start: Date, end: Date) {
        //
    }

    override fun onMissedCall(ctx: Context, number: String, start: Date) {
        //
    }

}