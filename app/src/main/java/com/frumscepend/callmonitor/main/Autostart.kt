package com.frumscepend.callmonitor.main

import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import com.frumscepend.callmonitor.main.service.ListenerService

class Autostart: BroadcastReceiver(){
    override fun onReceive(context: Context?, intent: Intent?) {
        val serviceIntent = Intent(context, ListenerService::class.java)
        context?.startService(serviceIntent)
    }

}