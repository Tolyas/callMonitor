package com.frumscepend.callmonitor.main.adapter

import android.net.Uri
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.BaseAdapter
import android.widget.FrameLayout
import android.widget.TextView
import com.frumscepend.callmonitor.R
import nl.changer.audiowife.AudioWife
import java.io.File

class SoundPlayerAdapter(private val fileList: ArrayList<String>) : BaseAdapter() {

    override fun getCount(): Int {
        return fileList.size
    }

    override fun getItem(i: Int): Any? {
        return null
    }

    override fun getItemId(i: Int): Long {
        return 0
    }

    override fun getView(i: Int, view: View?, viewGroup: ViewGroup): View? {
        val rootView: View = view ?: View.inflate(viewGroup.context, R.layout.item_sound_player, null)
        val file = File(fileList[i])
        rootView.findViewById<TextView>(R.id.textViewTitle).text = file.name
        val audioWife = AudioWife()
        audioWife.init(viewGroup.context, Uri.fromFile(file))
                .useDefaultUi(rootView.findViewById<FrameLayout>(R.id.playerView), LayoutInflater.from(viewGroup.context))
        return rootView
    }
}
