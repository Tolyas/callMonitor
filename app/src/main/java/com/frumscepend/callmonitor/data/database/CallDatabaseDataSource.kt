package com.frumscepend.callmonitor.data.database

import android.support.annotation.VisibleForTesting
import com.frumscepend.callmonitor.data.CallDataSource
import com.frumscepend.callmonitor.data.CallModel
import com.frumscepend.callmonitor.utils.AppExecutors
import java.util.*
import kotlin.collections.ArrayList


class CallDatabaseDataSource private constructor(
        private val appExecutors: AppExecutors,
        private val callsDao: CallsDao
): CallDataSource{
    override fun getCallsBeforeDate(providedDate: Long, callback: CallDataSource.CallsCallBack) {
        appExecutors.networkIO.execute {
            val calls = callsDao.getCallsBeforeDate(providedDate)
            appExecutors.mainThread.execute {
                if (calls.isEmpty()) {
                    callback.onFail()
                } else {
                    callback.onSuccess(calls)
                }
            }
        }
    }
    override fun getCallsName(name: String, duration: Int, callback: CallDataSource.CallsCallBack) {
        appExecutors.networkIO.execute {
            val calls = callsDao.getCallsName(name, duration)
            appExecutors.mainThread.execute {
                if (calls.isEmpty()) {
                    callback.onFail()
                } else {
                    callback.onSuccess(calls)
                }
            }
        }
    }

    override fun getCalls(callback: CallDataSource.CallsCallBack) {
        appExecutors.networkIO.execute {
            val calls = callsDao.getAllCalls()
            appExecutors.mainThread.execute {
                if (calls.isEmpty()) {
                    callback.onFail()
                } else {
                    callback.onSuccess(calls)
                }
            }
        }
    }

    override fun getCallsFromDate(providedDate: Long, callback: CallDataSource.CallsCallBack) {
        appExecutors.networkIO.execute {
            val calls = callsDao.getCallsFromDate(providedDate)
            appExecutors.mainThread.execute {
                if (calls.isEmpty()) {
                    callback.onFail()
                } else {
                    callback.onSuccess(calls)
                }
            }
        }
    }

    override fun updateCalls(call: CallModel) {
        appExecutors.networkIO.execute {
            callsDao.updateCall(call)
        }
    }

    override fun setCalls(calls: List<CallModel>) {
        appExecutors.networkIO.execute {
            val prevCalls = callsDao.getAllCalls()
            val callsResult = ArrayList<CallModel>()
            for (prevCall in prevCalls) {
                var found = false
                for (call in calls) {
                    if (call == prevCall) {
                        found = true
                    }
                }
                if (!found && !prevCall.deleted) {
                    prevCall.deleted = true
                    prevCall.deletedDate = Date().time
                }
            }
            callsResult.addAll(prevCalls)
            for (call in calls) {
                var found = false
                for (prevCall in prevCalls) {
                    if (call == prevCall) {
                        found = true
                    }
                }
                if (!found) {
                    callsResult.add(call)
                }
            }
            callsDao.insertAllCalls(callsResult)
        }
    }

    override fun getPendingCalls(callback: CallDataSource.CallsCallBack) {

    }

    companion object {
        private var INSTANCE: CallDataSource? = null

        @JvmStatic
        fun getInstance(appExecutors: AppExecutors, callsDao: CallsDao): CallDataSource {
            if (INSTANCE == null) {
                synchronized(CallDatabaseDataSource::javaClass) {
                    INSTANCE = CallDatabaseDataSource(appExecutors, callsDao)
                }
            }
            return INSTANCE!!
        }

        @VisibleForTesting
        fun clearInstance() {
            INSTANCE = null
        }
    }

}