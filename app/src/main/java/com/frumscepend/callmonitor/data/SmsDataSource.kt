package com.frumscepend.callmonitor.data

interface SmsDataSource {
    interface SmsListCallBack{
        fun onSuccess(smsList: List<SmsModel>)
        fun onFail()
    }

    fun getSmsList(callback: SmsListCallBack)

    fun getSmsListFromDate(providedDate: Long, callback: SmsListCallBack)

    fun setSmsList(smsList: List<SmsModel>)

    fun updateSms(sms: SmsModel)

    fun getPendingSmsList(callback: SmsListCallBack)
}