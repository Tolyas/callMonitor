package com.frumscepend.callmonitor.data.database

import android.arch.persistence.room.Database
import android.arch.persistence.room.Room
import android.arch.persistence.room.RoomDatabase
import android.content.Context
import com.frumscepend.callmonitor.data.CallModel
import com.frumscepend.callmonitor.data.SmsModel

/**
 * The Room MyDatabase that contains the Task table.
 */
@Database(entities = [(CallModel::class), (SmsModel::class)], version = 1)
abstract class MyDatabase : RoomDatabase() {

    abstract fun callsDao(): CallsDao

    abstract fun smsDao(): SmsDao

    companion object {

        private var INSTANCE: MyDatabase? = null

        private val lock = Any()

        fun getInstance(context: Context): MyDatabase {
            synchronized(lock) {
                if (INSTANCE == null) {
                    INSTANCE = Room.databaseBuilder(context.applicationContext,
                            MyDatabase::class.java, "CallMonitor.db")
                            .build()
                }
                return INSTANCE!!
            }
        }
    }

}