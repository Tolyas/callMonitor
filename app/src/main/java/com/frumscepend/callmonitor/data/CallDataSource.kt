package com.frumscepend.callmonitor.data

interface CallDataSource {
    interface CallsCallBack{
        fun onSuccess(calls: List<CallModel>)
        fun onFail()
    }

    fun getCalls(callback: CallsCallBack)

    fun getCallsFromDate(providedDate: Long, callback: CallsCallBack)

    fun getCallsBeforeDate(providedDate: Long, callback: CallsCallBack)

    fun getCallsName(name: String, duration: Int, callback: CallsCallBack)

    fun updateCalls(call: CallModel)

    fun setCalls(calls: List<CallModel>)

    fun getPendingCalls(callback: CallsCallBack)
}