package com.frumscepend.callmonitor.api

import com.frumscepend.callmonitor.data.CallModel
import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

class CallApiModel {
    @SerializedName("key")
    @Expose
    var key: String? = null
    @SerializedName("act")
    @Expose
    var act: String? = "add"
    @SerializedName("item_id")
    @Expose
    var itemId:Int? = null
    @SerializedName("phone_number")
    @Expose
    var phoneNumber: String? = null
    @SerializedName("name")
    @Expose
    var name: String? = null
    @SerializedName("type")
    @Expose
    var type: Int? = null
    @SerializedName("date")
    @Expose
    var date: Long? = null
    @SerializedName("duration")
    @Expose
    var duration: Int? = null
    @SerializedName("deleted_date")
    @Expose
    var deletedDate: Long? = null

    fun create(callModel: CallModel) {
        phoneNumber = callModel.phoneNumber
        name = callModel.name
        type = callModel.type
        date = callModel.date
        duration = callModel.duration
        deletedDate = callModel.deletedDate
        itemId = callModel.uid?.toInt()
    }
}