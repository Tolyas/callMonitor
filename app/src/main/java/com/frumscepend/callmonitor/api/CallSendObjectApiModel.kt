package com.frumscepend.callmonitor.api

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

class CallSendObjectApiModel {

    @SerializedName("calls_count")
    @Expose
    var callsCount: Int? = null
    @SerializedName("call")
    @Expose
    var call: List<CallSendApiModel>? = null

}