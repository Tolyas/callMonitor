package com.frumscepend.callmonitor.api

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

class ResponseApiModel {
    @SerializedName("id")
    @Expose
    var id:String? = null
}