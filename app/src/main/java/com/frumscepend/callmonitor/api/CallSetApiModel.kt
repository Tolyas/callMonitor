package com.frumscepend.callmonitor.api

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

class CallSetApiModel {

    @SerializedName("act")
    @Expose
    var act: String? = null
    @SerializedName("key")
    @Expose
    var key: String? = null
    @SerializedName("call_done")
    @Expose
    var callDone: List<Int>? = null

}