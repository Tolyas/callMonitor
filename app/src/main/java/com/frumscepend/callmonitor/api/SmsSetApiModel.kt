package com.frumscepend.callmonitor.api

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

class SmsSetApiModel {

    @SerializedName("act")
    @Expose
    var act: String? = null
    @SerializedName("key")
    @Expose
    var key: String? = null
    @SerializedName("sms_sent")
    @Expose
    var smsSent: List<Int>? = null

}