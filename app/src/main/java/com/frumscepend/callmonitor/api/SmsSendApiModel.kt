package com.frumscepend.callmonitor.api

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

class SmsSendApiModel {

    @SerializedName("smsid")
    @Expose
    var smsid: Int? = null
    @SerializedName("phone")
    @Expose
    var phone: String = ""
    @SerializedName("text")
    @Expose
    var text: String = ""

}