package com.frumscepend.callmonitor.api

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

class SmsRequestApiModel {

    @SerializedName("act")
    @Expose
    var act: String? = null
    @SerializedName("key")
    @Expose
    var key: String? = null

}