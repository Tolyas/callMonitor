package com.frumscepend.callmonitor.api

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

class IncomigCallAlert {
    @SerializedName("key")
    @Expose
    var key: String? = null
    @SerializedName("act")
    @Expose
    var act: String? = "incoming_call"
    @SerializedName("phone_number")
    @Expose
    var phoneNumber: String? = null
}