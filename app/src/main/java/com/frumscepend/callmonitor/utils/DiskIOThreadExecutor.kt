package com.frumscepend.callmonitor.utils

import java.util.concurrent.Executor
import java.util.concurrent.Executors

/**
 * Executor of new background thread.
 */
class DiskIOThreadExecutor : Executor {

    private val diskIO = Executors.newSingleThreadExecutor()

    override fun execute(command: Runnable) {
        diskIO.execute(command)
    }
}